import {
  TableContainer,
  Table,
  Thead,
  Th,
  Tr,
  Tbody,
  Td,
} from "@chakra-ui/react";

import { funcionario } from "../../@types/funcionario";
import { BtnDelete } from "../Buttons/Delete/Index";
import { BtnEdit } from "../Buttons/Edit/Index";

type Props = {
  funcionarios: funcionario[];
};

export function CustomTable(props: Props) {
  return (
    <div>
      <TableContainer border="solid 1x black">
        <Table variant="striped" colorScheme="gray">
          <Thead>
            <Tr>
              <Th>CPF</Th>
              <Th>Nome</Th>
              <Th>Setor</Th>
              <Th textAlign={"center"}>Ações</Th>
            </Tr>
          </Thead>
          <Tbody>
            {props.funcionarios.map((funcionario: funcionario) => (
              <Tr key={funcionario.id_funcionario}>
                <Td>{funcionario.nu_cpf}</Td>
                <Td>{funcionario.no_funcionario}</Td>
                <Td>{funcionario.gn_setor}</Td>
                <Td display={"flex"} gap={4}>
                  <BtnEdit id={funcionario.id_funcionario} />
                  <BtnDelete id={funcionario.id_funcionario} />
                </Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </div>
  );
}
