import { Button } from "@chakra-ui/react";

import { Trash } from "phosphor-react";
import { api } from "../../../services/api";

type Props = {
  id: number;
};

export function BtnDelete({ id }: Props) {
  function handleClick(id: number) {
    api
      .delete(`/${id}`)
      .then((res) => {
        alert("Deletado com sucesso");
        location.reload();
      })
      .catch(() => {
        alert("Erro ao deletar");
      });
  }

  return (
    <Button borderRadius={8} colorScheme="red" onClick={() => handleClick(id)}>
      <Trash size={20} />
    </Button>
  );
}
