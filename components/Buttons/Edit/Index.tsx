import { Button } from "@chakra-ui/react";
import { useRouter } from "next/router";

import { NotePencil } from "phosphor-react";

type Props = {
  id: number;
};

export function BtnEdit({ id }: Props) {
  const router = useRouter();

  function handleClick(id: number) {
    router.push(`/edit/${id}`);
  }

  return (
    <div>
      <Button
        borderRadius={8}
        colorScheme="yellow"
        onClick={() => handleClick(id)}
      >
        <NotePencil size={20} />
      </Button>
    </div>
  );
}
