import {
  FormControl,
  FormLabel,
  Select,
  Input,
  ButtonGroup,
  Button,
  Link,
} from "@chakra-ui/react";
import NextLink from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { funcionario } from "../../../@types/funcionario";
import { api } from "../../../services/api";

export function FormRegister() {
  const router = useRouter();
  const [nome, setNome] = useState("");
  const [cpf, setCpf] = useState("");
  const [pis, setPis] = useState("");
  const [setor, setSetor] = useState("");
  const [telefone1, setTelefone1] = useState("");
  const [telefone2, setTelefone2] = useState("");

  function hendleClickSubmit(
    nome: string,
    cpf: string,
    pis: string,
    setor: string,
    telefone1: string,
    telefone2: string
  ) {
    api
      .post<funcionario>("/", {
        no_funcionario: nome,
        nu_cpf: cpf,
        nu_pis: pis,
        gn_setor: setor,
        nu_telefone: [telefone1, telefone2],
      })
      .then((res) => {
        alert("Cadastro realizado com sucesso!");
        router.push("/");
      })
      .catch((err) => {
        alert("Erro ao realizar o cadastro!");
      });
  }
  return (
    <div>
      <FormControl display="flex" gap={8} flexDirection="row" maxWidth="960px">
        <div>
          <FormLabel>Nome:</FormLabel>
          <Input
            borderColor={"gray"}
            type="text"
            mb={4}
            onChange={(e) => setNome(e.target.value)}
          />
          <FormLabel>CPF:</FormLabel>
          <Input
            borderColor={"gray"}
            type="text"
            mb={4}
            onChange={(e) => setCpf(e.target.value)}
          />
          <FormLabel>PIS:</FormLabel>
          <Input
            borderColor={"gray"}
            type="text"
            onChange={(e) => setPis(e.target.value)}
          />
        </div>
        <div>
          <FormLabel>SETOR:</FormLabel>
          <Select
            borderColor={"gray"}
            placeholder="Selecione um Setor"
            mb={4}
            onChange={(e) => setSetor(e.target.value)}
          >
            <option value={"ADMINISTRATIVO"}>Administrativo</option>
            <option value={"ESCRITORIO"}>Escritório</option>
            <option value={"ESTOQUE"}>Estoque</option>
            <option value={"VENDAS"}>Vendas</option>
          </Select>
          <FormLabel>Telefone 1:</FormLabel>
          <Input
            borderColor={"gray"}
            type="text"
            mb={4}
            onChange={(e) => setTelefone1(e.target.value)}
          />
          <FormLabel>Telefone 2:</FormLabel>
          <Input
            borderColor={"gray"}
            type="text"
            onChange={(e) => setTelefone2(e.target.value)}
          />
        </div>
      </FormControl>

      <ButtonGroup mt={10} spacing="6">
        <Button
          w={100}
          colorScheme="green"
          onClick={() =>
            hendleClickSubmit(nome, cpf, pis, setor, telefone1, telefone2)
          }
        >
          Save
        </Button>
        <NextLink href="/" passHref>
          <Link textDecoration={"none"}>
            <Button w={100}>Cancelar</Button>
          </Link>
        </NextLink>
      </ButtonGroup>
    </div>
  );
}
