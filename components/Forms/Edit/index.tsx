import {
  FormControl,
  FormLabel,
  Select,
  Input,
  ButtonGroup,
  Button,
  Link,
} from "@chakra-ui/react";
import NextLink from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { funcionario } from "../../../@types/funcionario";
import { api } from "../../../services/api";

type Props = {
  id: number;
};

export function FormEdit({ id }: Props) {
  const [nome, setNome] = useState("");
  const [cpf, setCpf] = useState("");
  const [pis, setPis] = useState("");
  const [setor, setSetor] = useState("");
  const [telefone1, setTelefone1] = useState("");
  const [telefone2, setTelefone2] = useState("");

  const router = useRouter();

  useEffect(() => {
    api
      .get<funcionario>(`/${id}`)
      .then((res) => {
        setNome(res.data.no_funcionario);
        setCpf(res.data.nu_cpf);
        setPis(res.data.nu_pis);
        setSetor(res.data.gn_setor);
        setTelefone1(res.data.nu_telefone[0]);
        setTelefone2(res.data.nu_telefone[1]);
      })
      .catch(() => {
        alert("Erro ao buscar");
      });
  }, []);

  function handleClickSave(
    nome: string,
    cpf: string,
    pis: string,
    setor: string,
    telefone1: string,
    telefone2: string
  ) {
    api
      .put(`/${id}`, {
        no_funcionario: nome,
        nu_cpf: cpf,
        nu_pis: pis,
        gn_setor: setor,
        nu_telefone: [telefone1, telefone2],
      })
      .then(() => {
        alert("Salvo com sucesso");
        router.push("/");
      })
      .catch(() => {
        alert("Erro ao salvar");
      });
  }

  return (
    <div>
      <FormControl display="flex" gap={8} flexDirection="row" maxWidth="960px">
        <div>
          <FormLabel>Nome:</FormLabel>
          <Input
            borderColor={"gray"}
            type="text"
            mb={4}
            value={nome}
            onChange={(e) => setNome(e.target.value)}
          />
          <FormLabel>CPF:</FormLabel>
          <Input
            borderColor={"gray"}
            type="text"
            mb={4}
            value={cpf}
            disabled
            onChange={(e) => setCpf(e.target.value)}
          />
          <FormLabel>PIS:</FormLabel>
          <Input
            borderColor={"gray"}
            type="text"
            value={pis}
            disabled
            onChange={(e) => setPis(e.target.value)}
          />
        </div>
        <div>
          <FormLabel>SETOR:</FormLabel>
          <Select
            borderColor={"gray"}
            placeholder="Selecione um Setor"
            mb={4}
            value={setor}
            onChange={(e) => setSetor(e.target.value)}
          >
            <option value={"ADMINISTRATIVO"}>Administrativo</option>
            <option value={"ESCRITORIO"}>Escritório</option>
            <option value={"ESTOQUE"}>Estoque</option>
            <option value={"VENDAS"}>Vendas</option>
          </Select>
          <FormLabel>Telefone 1:</FormLabel>
          <Input
            borderColor={"gray"}
            type="text"
            mb={4}
            value={telefone1}
            onChange={(e) => setTelefone1(e.target.value)}
          />
          <FormLabel>Telefone 2:</FormLabel>
          <Input
            borderColor={"gray"}
            type="text"
            value={telefone2}
            onChange={(e) => setTelefone2(e.target.value)}
          />
        </div>
      </FormControl>

      <ButtonGroup mt={10} spacing="6">
        <Button
          w={100}
          colorScheme="green"
          onClick={() =>
            handleClickSave(nome, cpf, pis, setor, telefone1, telefone2)
          }
        >
          Salvar
        </Button>
        <NextLink href="/" passHref>
          <Link textDecoration={"none"}>
            <Button w={100}>Cancelar</Button>
          </Link>
        </NextLink>
      </ButtonGroup>
    </div>
  );
}
