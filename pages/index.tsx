import { Text, Button, Link } from "@chakra-ui/react";
import NextLink from "next/link";
import type { NextPage } from "next";
import { CustomTable } from "../components/CustomTable/CustomTable";
import styles from "../styles/Home.module.css";

import { useState, useEffect } from "react";

import { api } from "../services/api";
import { funcionario } from "../@types/funcionario";

const Home: NextPage = () => {
  const [funcionarios, setFuncionarios] = useState<funcionario[]>([]);

  useEffect(() => {
    api.get<funcionario[]>("/").then((response) => {
      setFuncionarios(response.data);
    });
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <Text fontSize={28} fontWeight="500">
          {" "}
          Lista de Funcionários{" "}
        </Text>
        <div className={styles.header}>
          <NextLink href="/cadastro" passHref>
            <Link>
              <Button colorScheme="green">Cadastrar</Button>
            </Link>
          </NextLink>
        </div>

        <div></div>
        <CustomTable funcionarios={funcionarios} />
      </div>
    </div>
  );
};

export default Home;
