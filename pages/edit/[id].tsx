import { useRouter } from "next/router";
import { FormEdit } from "../../components/Forms/Edit";

function Edit() {
  const { query } = useRouter();
  return <FormEdit id={Number(query.id)} />;
}

export default Edit;
