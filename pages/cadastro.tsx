import styles from "../styles/Cadastro.module.css";
import { Text } from "@chakra-ui/react";
import { FormRegister } from "../components/Forms/Register";

function Cadastro() {
  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <Text fontSize={28} fontWeight="500">
          Cadastro
        </Text>
        <FormRegister />
      </div>
    </div>
  );
}

export default Cadastro;
