export type funcionario = {
    id_funcionario : number;
    nu_cpf : string;
    no_funcionario : string;
    nu_pis : string;
    gn_setor : string;
    nu_telefone : string[];
}